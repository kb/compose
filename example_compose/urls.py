# -*- encoding: utf-8 -*-
from django.conf import settings
from django.urls import include, re_path
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView

from .views import DashView, SettingsView


urlpatterns = [
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(
            url=reverse_lazy("block.page.list"), permanent=False
        ),
        name="project.dash",
    ),
    re_path(r"^dash/$", view=DashView.as_view(), name="project.dash"),
    re_path(
        r"^settings/$", view=SettingsView.as_view(), name="project.settings"
    ),
    re_path(r"^block/", view=include("block.urls.block")),
    re_path(r"^compose/", view=include("compose.urls.compose")),
    re_path(r"^gallery/", view=include("gallery.urls")),
    re_path(r"^snippet/", view=include("compose.urls.snippet")),
    re_path(r"^wizard/", view=include("block.urls.wizard")),
    # this url include should come last
    re_path(r"^", view=include("block.urls.cms")),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
