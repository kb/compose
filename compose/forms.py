# -*- encoding: utf-8 -*-
from base.form_utils import bleach_clean, RequiredFieldForm
from block.forms import ContentBaseForm, ContentEmptyForm
from .models import (
    Article,
    Calendar,
    CodeSnippet,
    Feature,
    FeatureStyle,
    Header,
    HeaderStyle,
    Map,
    Sidebar,
    Slideshow,
)


class ArticleForm(ContentBaseForm):
    def __init__(self, *args, **kwargs):
        # default section fields for article does not include the slug field
        section_fields = kwargs.pop(
            "field_list", ["title", "description", "article_type", "image_size"]
        )
        super().__init__(*args, **kwargs)
        for name in ("title", "description"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})
        self._setup_fields(section_fields)

    def clean_description(self):
        data = self.cleaned_data["description"]
        return bleach_clean(data)

    class Meta:
        model = Article
        fields = ("slug", "title", "description", "article_type", "image_size")


class CalendarForm(ContentEmptyForm):
    class Meta:
        model = Calendar
        fields = ()


class CodeSnippetCreateForm(RequiredFieldForm):
    class Meta:
        model = CodeSnippet
        fields = ("slug", "code")


class CodeSnippetUpdateForm(CodeSnippetCreateForm):
    class Meta:
        model = CodeSnippet
        fields = ("code",)


class SlideshowForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super(SlideshowForm, self).__init__(*args, **kwargs)
        for name in ("title", "description"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    def clean_description(self):
        data = self.cleaned_data["description"]
        return bleach_clean(data)

    class Meta:
        model = Slideshow
        fields = ("title", "description")


class FeatureForm(ContentBaseForm):
    def __init__(self, *args, **kwargs):
        section_fields = kwargs.pop("field_list", None)
        super(FeatureForm, self).__init__(*args, **kwargs)
        for name in ("title", "description"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"}),
        self.fields["title"].widget.attrs.update({"rows": "1"})
        self._setup_fields(section_fields)

    class Meta:
        model = Feature
        fields = ("title", "description", "style")


class FeatureStyleForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super(FeatureStyleForm, self).__init__(*args, **kwargs)
        for name in ("name", "css_class_name"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = FeatureStyle
        fields = ("name", "css_class_name")


class HeaderForm(ContentBaseForm):
    def __init__(self, *args, **kwargs):
        section_fields = kwargs.pop("field_list", None)
        super(HeaderForm, self).__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["title"].widget.attrs.update({"rows": "3"})
        self._setup_fields(section_fields)

    def clean_title(self):
        data = self.cleaned_data["title"]
        return bleach_clean(data)

    class Meta:
        model = Header
        fields = ("title", "style")


class HeaderStyleForm(RequiredFieldForm):
    def __init__(self, *args, **kwargs):
        super(HeaderStyleForm, self).__init__(*args, **kwargs)
        for name in ("name", "css_class_name"):
            self.fields[name].widget.attrs.update({"class": "pure-input-2-3"})

    class Meta:
        model = HeaderStyle
        fields = ("name", "css_class_name")


class MapForm(ContentEmptyForm):
    class Meta:
        model = Map
        fields = ()


class SidebarForm(ContentBaseForm):
    def __init__(self, *args, **kwargs):
        section_fields = kwargs.pop("field_list", None)
        super().__init__(*args, **kwargs)
        self.fields["title"].widget.attrs.update({"class": "pure-input-2-3"})
        self.fields["title"].widget.attrs.update({"rows": "3"})
        self._setup_fields(section_fields)

    def clean_title(self):
        data = self.cleaned_data["title"]
        return bleach_clean(data)

    class Meta:
        model = Sidebar
        fields = ("title",)
