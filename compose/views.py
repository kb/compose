# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin
from django.urls import reverse
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from base.view_utils import BaseMixin
from block.forms import ContentEmptyForm
from block.views import (
    CMSRoleRequiredMixin,
    ContentCreateView,
    ContentPublishView,
    ContentRemoveView,
    ContentUpdateView,
)

from .forms import (
    ArticleForm,
    CalendarForm,
    CodeSnippetCreateForm,
    CodeSnippetUpdateForm,
    FeatureForm,
    FeatureStyleForm,
    HeaderForm,
    HeaderStyleForm,
    MapForm,
    SidebarForm,
    SlideshowForm,
)
from .models import (
    Article,
    ArticleBlock,
    Calendar,
    CalendarBlock,
    CodeSnippet,
    Feature,
    FeatureBlock,
    FeatureStyle,
    Header,
    HeaderBlock,
    HeaderStyle,
    Map,
    MapBlock,
    Sidebar,
    SidebarBlock,
    Slideshow,
    SlideshowBlock,
)


class ArticleCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = ArticleBlock
    form_class = ArticleForm
    model = Article
    template_name = "compose/article_create.html"


class ArticlePublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Article
    template_name = "compose/article_publish.html"

    def form_valid(self, form):
        result = super().form_valid(form)
        if self.object:
            # create a url for any articles with a slug
            self.object.update_urls()
        return result


class ArticleRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Article
    template_name = "compose/article_remove.html"


class ArticleUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentUpdateView
):

    form_class = ArticleForm
    model = Article
    template_name = "compose/article_update.html"


class CalendarCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = CalendarBlock
    form_class = CalendarForm
    model = Calendar
    template_name = "compose/calendar_form.html"


class CalendarPublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Calendar
    template_name = "compose/calendar_publish.html"


class CalendarRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Calendar
    template_name = "compose/calendar_remove.html"


class CodeSnippetCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, CreateView
):

    form_class = CodeSnippetCreateForm
    model = CodeSnippet

    def get_success_url(self):
        return reverse("compose.code.snippet.list")


class CodeSnippetListView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, ListView
):

    model = CodeSnippet
    paginate_by = 15


class CodeSnippetUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, BaseMixin, UpdateView
):

    form_class = CodeSnippetUpdateForm
    model = CodeSnippet

    def get_success_url(self):
        return reverse("compose.code.snippet.list")


class FeatureCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = FeatureBlock
    form_class = FeatureForm
    model = Feature
    template_name = "compose/feature_create_update.html"


class FeaturePublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Feature
    template_name = "compose/feature_publish.html"


class FeatureRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Feature
    template_name = "compose/feature_remove.html"


class FeatureUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentUpdateView
):

    form_class = FeatureForm
    model = Feature
    template_name = "compose/feature_create_update.html"


class FeatureStyleCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, CreateView
):
    form_class = FeatureStyleForm
    model = FeatureStyle
    template_name = "compose/style_create_update.html"

    def get_success_url(self):
        return reverse("compose.feature.style.list")


class FeatureStyleUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, UpdateView
):
    form_class = FeatureStyleForm
    model = FeatureStyle
    template_name = "compose/style_create_update.html"

    def get_success_url(self):
        return reverse("compose.feature.style.list")


class HeaderCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = HeaderBlock
    form_class = HeaderForm
    model = Header
    template_name = "compose/header_create_update.html"


class HeaderPublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Header
    template_name = "compose/header_publish.html"


class HeaderUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentUpdateView
):

    form_class = HeaderForm
    model = Header
    template_name = "compose/header_create_update.html"


class HeaderRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Header
    template_name = "compose/header_remove.html"


class HeaderStyleCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, CreateView
):
    form_class = HeaderStyleForm
    model = HeaderStyle
    template_name = "compose/style_create_update.html"

    def get_success_url(self):
        return reverse("compose.header.style.list")


class HeaderStyleUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, UpdateView
):
    form_class = HeaderStyleForm
    model = HeaderStyle
    template_name = "compose/style_create_update.html"

    def get_success_url(self):
        return reverse("compose.header.style.list")


class FeatureStyleListView(LoginRequiredMixin, CMSRoleRequiredMixin, ListView):
    model = FeatureStyle


class HeaderStyleListView(LoginRequiredMixin, CMSRoleRequiredMixin, ListView):
    model = HeaderStyle


class MapCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = MapBlock
    form_class = MapForm
    model = Map
    template_name = "compose/map_form.html"


class MapPublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Map
    template_name = "compose/map_publish.html"


class MapRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Map
    template_name = "compose/map_remove.html"


class SidebarCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = SidebarBlock
    form_class = SidebarForm
    model = Sidebar
    template_name = "compose/sidebar_form.html"


class SidebarPublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Sidebar
    template_name = "compose/sidebar_publish.html"


class SidebarRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Sidebar
    template_name = "compose/sidebar_remove.html"


class SidebarUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentUpdateView
):

    form_class = SidebarForm
    model = Sidebar
    template_name = "compose/sidebar_form.html"


class SlideshowCreateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentCreateView
):

    block_class = SlideshowBlock
    form_class = SlideshowForm
    model = Slideshow
    template_name = "compose/slideshow_create.html"


class SlideshowPublishView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentPublishView
):

    form_class = ContentEmptyForm
    model = Slideshow
    template_name = "compose/slideshow_publish.html"


class SlideshowRemoveView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentRemoveView
):

    form_class = ContentEmptyForm
    model = Slideshow
    template_name = "compose/slideshow_remove.html"


class SlideshowUpdateView(
    LoginRequiredMixin, CMSRoleRequiredMixin, ContentUpdateView
):

    form_class = SlideshowForm
    model = Slideshow
    template_name = "compose/slideshow_update.html"
