# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from block.models import Page, Section, Template, TemplateSection, Url
from compose.models import (
    SECTION_BODY,
    SECTION_CARD,
    SECTION_GALLERY,
    SECTION_SLIDESHOW,
)


class Command(BaseCommand):

    ARTICLE = "Article"
    help = "Initialise 'compose' application"

    def init_app_compose(self):
        # section - body
        body = Section.objects.init_section(
            SECTION_BODY,
            SECTION_BODY.capitalize(),
            "compose",
            "Article",
            "compose.article.create",
        )
        # section - card
        card = Section.objects.init_section(
            SECTION_CARD,
            SECTION_CARD.capitalize(),
            "compose",
            "Article",
            "compose.article.create",
        )
        # section - gallery
        gallery = Section.objects.init_section(
            SECTION_GALLERY,
            SECTION_GALLERY.capitalize(),
            "compose",
            "Slideshow",
            "compose.slideshow.create",
        )
        # section - slideshow
        show = Section.objects.init_section(
            SECTION_SLIDESHOW,
            SECTION_SLIDESHOW.capitalize(),
            "compose",
            "Slideshow",
            "compose.slideshow.create",
        )
        # template - article
        article_template = Template.objects.init_template(
            self.ARTICLE, "compose/page_article.html"
        )
        TemplateSection.objects.init_template_section(article_template, body)
        TemplateSection.objects.init_template_section(article_template, card)
        TemplateSection.objects.init_template_section(article_template, show)
        # template - gallery
        gallery_template = Template.objects.init_template(
            "Gallery", "compose/page_gallery.html"
        )
        TemplateSection.objects.init_template_section(gallery_template, gallery)
        TemplateSection.objects.init_template_section(gallery_template, show)
        # page - home
        home = Page.objects.init_page(
            Page.HOME, "", "Home", 0, article_template, is_home=True
        )
        home.refresh_sections_from_template()
        # page - gallery - hidden on init
        gallery_page = Page.objects.init_page(
            "gallery", "", "Gallery", 0, gallery_template
        )
        gallery_page.refresh_sections_from_template()
        # Link wizard
        Url.objects.init_pages()

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        self.init_app_compose()
        self.stdout.write("{} - Complete".format(self.help))
