# -*- encoding: utf-8 -*-
import pytest

from block.tests.helper import check_content, check_field_list
from block.tests.factories import SectionFactory

from compose.tests.factories import FeatureFactory


@pytest.mark.django_db
def test_content_methods():
    c = FeatureFactory()
    check_content(c)


@pytest.mark.django_db
def test_feature_empty_field_list(client):
    check_field_list(
        client=client,
        factory=FeatureFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Feature",
            create_url_name="compose.feature.create",
            field_list="",
        ),
        expected_fields=["title", "description", "style"],
    )


@pytest.mark.django_db
def test_feature_field_list(client):
    check_field_list(
        client=client,
        factory=FeatureFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Feature",
            create_url_name="compose.feature.create",
            field_list="title description",
        ),
        expected_fields=["title", "description"],
    )
