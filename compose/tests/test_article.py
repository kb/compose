# -*- encoding: utf-8 -*-
import pytest

from block.tests.factories import SectionFactory
from block.tests.helper import check_content, check_field_list
from compose.tests.factories import ArticleFactory


@pytest.mark.django_db
def test_content_methods():
    c = ArticleFactory()
    check_content(c)


@pytest.mark.django_db
def test_article_empty_field_list(client):
    check_field_list(
        client=client,
        factory=ArticleFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Article",
            create_url_name="compose.article.create",
            field_list="",
        ),
        expected_fields=["title", "description", "article_type", "image_size"],
    )


@pytest.mark.django_db
def test_article_field_list(client):
    check_field_list(
        client=client,
        factory=ArticleFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Article",
            create_url_name="compose.article.create",
            field_list="title description",
        ),
        expected_fields=["title", "description"],
    )


@pytest.mark.django_db
def test_article_field_list_add_slug(client):
    check_field_list(
        client=client,
        factory=ArticleFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Article",
            create_url_name="compose.article.create",
            field_list="slug title description",
        ),
        expected_fields=["slug", "title", "description"],
    )
