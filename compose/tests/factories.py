# -*- encoding: utf-8 -*-
import factory

from block.tests.factories import PageSectionFactory
from compose.models import (
    Article,
    ArticleBlock,
    Calendar,
    CalendarBlock,
    CodeSnippet,
    Map,
    MapBlock,
    Sidebar,
    SidebarBlock,
    Slideshow,
    SlideshowBlock,
    SlideshowImage,
    Feature,
    FeatureBlock,
    Header,
    HeaderBlock,
)


class ArticleBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = ArticleBlock

    @factory.sequence
    def order(n):
        return n


class ArticleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Article

    block = factory.SubFactory(ArticleBlockFactory)


class CalendarBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = CalendarBlock

    @factory.sequence
    def order(n):
        return n


class CalendarFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Calendar

    block = factory.SubFactory(CalendarBlockFactory)


class CodeSnippetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CodeSnippet

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)


class FeatureBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = FeatureBlock

    @factory.sequence
    def order(n):
        return n


class FeatureFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Feature

    block = factory.SubFactory(FeatureBlockFactory)


class HeaderBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = HeaderBlock

    @factory.sequence
    def order(n):
        return n


class HeaderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Header

    block = factory.SubFactory(HeaderBlockFactory)


class MapBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = MapBlock

    @factory.sequence
    def order(n):
        return n


class MapFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Map

    block = factory.SubFactory(MapBlockFactory)


class SidebarBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = SidebarBlock

    @factory.sequence
    def order(n):
        return n


class SidebarFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Sidebar

    block = factory.SubFactory(SidebarBlockFactory)


class SlideshowBlockFactory(factory.django.DjangoModelFactory):

    page_section = factory.SubFactory(PageSectionFactory)

    class Meta:
        model = SlideshowBlock

    @factory.sequence
    def order(n):
        return n


class SlideshowImageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = SlideshowImage

    @factory.sequence
    def order(n):
        return n


class SlideshowFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Slideshow

    block = factory.SubFactory(SlideshowBlockFactory)
