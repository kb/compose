# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data():
    call_command("demo_data_compose")


@pytest.mark.django_db
def test_init_app():
    call_command("init_app_compose")


@pytest.mark.django_db
def test_init_app_news():
    call_command("init_app_compose")
    call_command("init_app_compose_news")
