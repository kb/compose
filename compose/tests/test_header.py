# -*- encoding: utf-8 -*-
import pytest

from block.tests.helper import check_content, check_field_list
from block.tests.factories import SectionFactory

from compose.tests.factories import HeaderFactory


@pytest.mark.django_db
def test_content_methods():
    c = HeaderFactory()
    check_content(c)


@pytest.mark.django_db
def test_header_empty_field_list(client):
    check_field_list(
        client=client,
        factory=HeaderFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Header",
            create_url_name="compose.header.create",
            field_list="",
        ),
        expected_fields=["title", "style"],
    )


@pytest.mark.django_db
def test_header_field_list(client):
    check_field_list(
        client=client,
        factory=HeaderFactory,
        section=SectionFactory(
            block_app="compose",
            block_model="Header",
            create_url_name="compose.header.create",
            field_list="title",
        ),
        expected_fields=["title"],
    )
