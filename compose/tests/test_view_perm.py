# -*- encoding: utf-8 -*-
from django.urls import reverse

from base.tests.test_utils import PermTestCase
from block.tests.factories import (
    LinkFactory,
    PageFactory,
    PageSectionFactory,
    PanelBlockFactory,
    SectionFactory,
    TemplateFactory,
)
from compose.tests.factories import (
    ArticleFactory,
    CalendarBlockFactory,
    CalendarFactory,
    CodeSnippetFactory,
    MapBlockFactory,
    MapFactory,
    SidebarFactory,
    SlideshowBlockFactory,
    SlideshowFactory,
    FeatureFactory,
)


class TestViewPerm(PermTestCase):
    def setUp(self):
        self.setup_users()

    def _test_block_create(self, url_name):
        """test the 4 incarnations of this create url.

        page slug, section slug
        page slug, page slug_menu, section slug
        page slug, panel pk, section slug
        page slug, page slug_menu, panel pk, section slug
        """
        home_page = PageFactory(slug="home", slug_menu="")
        about_page = PageFactory(slug="help", slug_menu="about")
        home_ps = PageSectionFactory(page=home_page)
        about_ps = PageSectionFactory(page=about_page)
        panel_section = SectionFactory(
            slug="panel", name="Panel", block_app="block", block_model="Panel"
        )
        home_panel = PanelBlockFactory(
            page_section=PageSectionFactory(
                page=home_page, section=panel_section
            )
        )
        about_panel = PanelBlockFactory(
            page_section=PageSectionFactory(
                page=about_page, section=panel_section
            )
        )

        url = reverse(
            url_name,
            kwargs=dict(page=home_page.slug, section=home_ps.section.slug),
        )
        self.assert_staff_only(url)

        url = reverse(
            url_name,
            kwargs=dict(
                page=about_page.slug,
                menu=about_page.slug_menu,
                section=about_ps.section.slug,
            ),
        )
        self.assert_staff_only(url)

        url = reverse(
            url_name,
            kwargs=dict(
                page=home_page.slug,
                panel=home_panel.pk,
                section=home_ps.section.slug,
            ),
        )
        self.assert_staff_only(url)

        url = reverse(
            url_name,
            kwargs=dict(
                page=about_page.slug,
                menu=about_page.slug_menu,
                panel=about_panel.pk,
                section=about_ps.section.slug,
            ),
        )
        self.assert_staff_only(url)

    def test_article_create(self):
        self._test_block_create("compose.article.create")

    def test_article_publish(self):
        c = ArticleFactory()
        url = reverse("compose.article.publish", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_article_remove(self):
        c = ArticleFactory()
        url = reverse("compose.article.remove", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_article_update(self):
        c = ArticleFactory()
        url = reverse("compose.article.update", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_calendar_create(self):
        self._test_block_create("compose.calendar.create")

    def test_calendar_publish(self):
        ps = PageSectionFactory(section=SectionFactory(slug="calendar"))
        c = CalendarFactory(
            block=CalendarBlockFactory(page_section=ps),
            calendar=LinkFactory(),
        )
        url = reverse("compose.calendar.publish", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_calendar_remove(self):
        ps = PageSectionFactory(section=SectionFactory(slug="calendar"))
        c = CalendarFactory(
            block=CalendarBlockFactory(page_section=ps),
            calendar=LinkFactory(),
        )
        url = reverse("compose.calendar.remove", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_code_snippet_list(self):
        url = reverse("compose.code.snippet.list")
        self.assert_staff_only(url)

    def test_code_snippet_create(self):
        url = reverse("compose.code.snippet.create")
        self.assert_staff_only(url)

    def test_code_snippet_update(self):
        snippet = CodeSnippetFactory()
        url = reverse("compose.code.snippet.update", args=[snippet.slug])
        self.assert_staff_only(url)

    def test_feature_create(self):
        self._test_block_create("compose.feature.create")

    def test_feature_publish(self):
        c = FeatureFactory()
        url = reverse("compose.feature.publish", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_feature_remove(self):
        c = FeatureFactory()
        url = reverse("compose.feature.remove", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_feature_update(self):
        c = FeatureFactory()
        url = reverse("compose.feature.update", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_home(self):
        template = TemplateFactory(template_name="compose/page_article.html")
        PageFactory(is_home=True, slug="home", slug_menu="", template=template)
        url = reverse("project.home")
        self.assert_any(url)

    def test_map_create(self):
        self._test_block_create("compose.map.create")

    def test_map_publish(self):
        ps = PageSectionFactory(section=SectionFactory(slug="map"))
        c = MapFactory(
            block=MapBlockFactory(page_section=ps),
            map=LinkFactory(),
        )
        url = reverse("compose.map.publish", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_map_remove(self):
        ps = PageSectionFactory(section=SectionFactory(slug="map"))
        c = MapFactory(
            block=MapBlockFactory(page_section=ps),
            map=LinkFactory(),
        )
        url = reverse("compose.map.remove", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_page_design_home(self):
        template = TemplateFactory(template_name="compose/page_article.html")
        p = PageFactory(slug="home", slug_menu="", template=template)
        PageSectionFactory(
            page=p,
            section=SectionFactory(
                slug="slideshow",
                block_app="compose",
                block_model="SlideShow",
                create_url_name="compose.slideshow.create",
            ),
        )
        PageSectionFactory(
            page=p,
            section=SectionFactory(
                slug="body",
                block_app="compose",
                block_model="Article",
                create_url_name="compose.article.create",
            ),
        )
        PageSectionFactory(
            page=p,
            section=SectionFactory(
                slug="card",
                block_app="compose",
                block_model="Article",
                create_url_name="compose.article.create",
            ),
        )
        url = reverse("project.page.design", kwargs=dict(page=p.slug))
        self.assert_staff_only(url)

    def test_sidebar_create(self):
        self._test_block_create("compose.sidebar.create")

    def test_sidebar_publish(self):
        c = SidebarFactory()
        url = reverse("compose.sidebar.publish", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_sidebar_remove(self):
        c = SidebarFactory()
        url = reverse("compose.sidebar.remove", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_sidebar_update(self):
        c = SidebarFactory()
        url = reverse("compose.sidebar.update", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_slideshow_create(self):
        self._test_block_create("compose.slideshow.create")

    def test_slideshow_publish(self):
        ps = PageSectionFactory(section=SectionFactory(slug="slideshow"))
        c = SlideshowFactory(block=SlideshowBlockFactory(page_section=ps))
        url = reverse("compose.slideshow.publish", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_slideshow_remove(self):
        c = SlideshowFactory()
        url = reverse("compose.slideshow.remove", kwargs={"pk": c.pk})
        self.assert_staff_only(url)

    def test_slideshow_update(self):
        c = SlideshowFactory()
        url = reverse("compose.slideshow.update", kwargs={"pk": c.pk})
        self.assert_staff_only(url)
