# -*- encoding: utf-8 -*-
from django.urls import re_path

from compose.views import (
    CodeSnippetCreateView,
    CodeSnippetListView,
    CodeSnippetUpdateView,
)


urlpatterns = [
    re_path(
        r"^code/snippet/$",
        view=CodeSnippetListView.as_view(),
        name="compose.code.snippet.list",
    ),
    re_path(
        r"^code/snippet/create/$",
        view=CodeSnippetCreateView.as_view(),
        name="compose.code.snippet.create",
    ),
    re_path(
        r"^code/snippet/(?P<slug>[-\w\d]+)/edit/$",
        view=CodeSnippetUpdateView.as_view(),
        name="compose.code.snippet.update",
    ),
]
