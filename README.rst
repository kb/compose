Compose
*******

Django application containing a couple of models for use with the block app
when building a simple CMS site.

The master branch allows ordering of block (if enabled in settings) and should
be used with the master branch of the block app.
The branch 962-legacy-block-order is retained to support projects that have
not been converted. At the time of writing the last release on that branch was
0.0.50.  It should be used with the 962-legacy block-order branch (release
0.2.05)

The conversion process is as follows::

  - release project with version 0.2.05 or the block app and 0.0.50 of the
    compose app and deploy to the site
  - run the management command 962-block-export-order
  - release the project with the latest version of the block and compose app
    from the master branch and deploy to the site
  - run the management command 962-block-import-order
  - if block ordering is required enable in blocksettings

.. important:: We will put the panel system into this app.

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-pipeline
  # or
  virtualenv --python=python3 venv-compose

  source venv-compose/bin/activate
  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
